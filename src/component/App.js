import React, { Component } from 'react'
import '../css/App.css'
import Sidebar from './Sidebar'
import TextField from './TextField';


class App extends Component {
    state = {
        key: undefined,
        heading: '',
        content: '',
        displayTextField: false
    }
    hideTextField = () => { this.setState({ displayTextField: false }); };
    showTextFiled = () => { this.setState({ displayTextField: true }) };
    selectNote = (obj) => {this.setState(obj);  }

    render() {
        const contextValue = {
            data: this.state,
            hide: this.hideTextField,
            show: this.showTextFiled,
            changeState: this.selectNote

        };
        return (
            <React.Fragment>
                <h1>Notes</h1>
                <Sidebar fundata={contextValue} />
                {this.state.displayTextField && <TextField fundata={contextValue} />}
            </React.Fragment>

        );
    }
}



export default App;
