import React, { Component } from 'react'
import '../css/App.css'

class TextField extends Component {
    constructor(props) {
        super(props);
        this.state = this.props.fundata.data;
    }

    headingChange = e => { this.setState({ heading: e.target.value }); };
    contentChange = e => { this.setState({ content: e.target.value }); };

    deleteNote = () => {
        if (this.state.key !== undefined) {
            let notelist = JSON.parse(localStorage.getItem("notelist"));
            delete notelist[this.state.key];
            localStorage.setItem("notelist", JSON.stringify(notelist));
            alert("note deleted succesfully");
        }
        this.props.fundata.hide();
    }

    saveNote = () => {
        this.props.fundata.hide();

        if (localStorage.getItem("notelist") == null) {
            localStorage.setItem("notelist", JSON.stringify({}));
        }
        let notelist = JSON.parse(localStorage.getItem("notelist"));
        if (this.state.key) {
            notelist[this.state.key] = { heading: this.state.heading, content: this.state.content };
        }
        else {
            notelist[Date()] = { heading: this.state.heading, content: this.state.content };
        }
        localStorage.setItem("notelist", JSON.stringify(notelist));
        this.props.fundata.hide();
        alert("note save successfully");
    }
    render() {

        return (
            <div className="container" id="container">
                <div>
                    <input id="heading" type="text" autoComplete="off" placeholder="Heading" value={this.state.heading} onChange={this.headingChange} />
                </div>
                <div>
                    <textarea id="content" placeholder="Write your notes-----" value={this.state.content} onChange={this.contentChange}></textarea>
                </div>
                <div>
                    <button id="delete" onClick={this.deleteNote}>Delete</button>
                    <button id="save" onClick={this.saveNote}>Save</button>
                </div>
            </div>
        );

    }
}

export default TextField;