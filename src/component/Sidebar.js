import React, { Component } from 'react'
import '../css/App.css'

class Sidebar extends Component {

    selectNote = e => {
        let notelist = JSON.parse(localStorage.getItem("notelist"));
        const key = e.target.id;
        this.props.fundata.changeState({ key: key, heading: notelist[key].heading, content: notelist[key].content, displayTextField: false });
        this.props.fundata.show();
    }

    addNote = () => {
        this.props.fundata.changeState({ key: undefined, heading: '', content: '', displayTextField: false });
        this.props.fundata.show();

    }

    render() {

        let noteArr = (() => {
            let arr = [];
            let notelist = JSON.parse(localStorage.getItem("notelist"));
            for (let key in notelist) { arr.push(<li key={key} id={key} onClick={this.selectNote}>{notelist[key].heading}</li>); }
            return arr;
        })();

        return (
            <aside className="slidebar">
                <ul id="noteslist">
                    {noteArr}
                </ul>
                <button id="addnote" onClick={this.addNote}>Add Notes</button>

            </aside>
        );

    }
}

export default Sidebar;